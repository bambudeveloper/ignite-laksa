# Ignite Laksa

This is the generator plugin to bring the Malaysian and Singaporean flavour to your ignite boilerpalte development.

This will be keyed in to work with the Bambu Mobile Boilerplate.

## Getting Started

To install, run the following command:

```js
yarn add git+https://bitbucket.org/bambudeveloper/ignite-laksa.git#0.0.1
```

For now, it will acquire the node modulevia a public repo from Bambu's bitbucket.

Once this has been field tested and proven, it will be an NPM package.

## Commands

Currently, there are two generator commands, namely:

1. LaksaComponent
1. LaksaContainer

**LaksaComponent**

This will generate a React component that is not connected to any external providers such as redux and saga. Rather it relies on a `container` which is usually it's parent to orchestrate its behavior.

To run this, simply:

```js
ignite g LaksaComponent <component name>
```

_If you haven't guessed, replace `<component name>` with the name you desire for your component._

**LaksaContainer**

Laksa container generates a connected React component, known as a container. This will be the mothership component and will be considered the screen the user sees.

To run this, simply:

```js
ignite g LaksaContainer <component name>
```

_If you haven't guessed, replace `<component name>` with the name you desire for your component._

This command will also inject this screen in the default stack navigator so, you, the coder, does not have to worrk about it.

There are additional flags here to generate additional items, namely:

| Flag    | Description                                                                               |
|---------|-------------------------------------------------------------------------------------------|
| -saga   | Will generate template saga file to facilitate saga functionality, usually for API linkup.|
| -redux  | Will generate a template redux file to your redux needs. It will also inject the redux into the global redux reducer so the coder does not have to worry about it.

For instance, if you want to have redux but no saga in your container, run the following:

```js
ignite g LaksaContainer <component name> -redux
```

If you want everything, run the following:

```js
ignite g LaksaContainer <component name> -redux -saga
```

This document will be a living document, this will be updated frequently. Stay tuned for features and fixes.

Have a spicy, laksa-licious day ;)
